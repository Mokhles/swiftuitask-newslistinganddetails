//
//  DateFormatter+extension.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 05/03/2023.
//

import Foundation

extension DateFormatter {
    enum Formats: String {
        case yyyyMMddTHHmmssZ = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
        case yyyyMMddhhmma = "yyyy-MM-dd hh:mm a"
        case yyyyMMddhhmmss = "yyyy-MM-dd HH:mm:ss"
        case yyyyMMdd = "yyyy-MM-dd"
        case dMMM = "d MMM"
        case MMMM = "MMMM"
        case MMM = "MMM"
        case HHmmss = "HH:mm:ss"
        case HHmm = "HH:mm"
        case hhmma = "hh:mm a"
        case ddMMMyyyy = "dd MMM. yyyy"
        case ddmmyyyy = "dd/MM/yyyy"
        case MMDDYY = "MM-dd-yyyy"
        case EEEEdMMMyyyy = "EEEE d MMM yyyy"
        case ddmmyyyyHHmmss = "dd/MM/yyyy HH:mm:ss"
        case dMMMyyy = "d MMM yyyy"
        case MMMdyyy = "MMM d, yyyy"
        case MMddyyyy = "MM/dd/yyyy"
        case dMMMMyyy = "d MMMM yyyy"
    }


    func date(fromString string: String, withFormat format: Formats) -> Date? {
        self.calendar = Calendar(identifier: .gregorian)
        self.dateFormat = format.rawValue
        return date(from: string)
    }
}

//
//  Enums.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 06/03/2023.
//

import Foundation

enum PageIndex {
    case first
    case next
}

//
//  APError.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 27/02/2023.
//

import Foundation

enum APError: Error {
    case invalidURL
    case unableToComplete
    case invalidResponse
    case invalidData
}

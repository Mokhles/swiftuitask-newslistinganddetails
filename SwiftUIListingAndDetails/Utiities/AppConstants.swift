//
//  AppConstants.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 06/03/2023.
//

import Foundation


var kAPIsPageSize: Int = 4
var thumbdDefaultImage = "https://ihfedudev-admin.linkdev.com/sites/default/files/news-images/2022-03/News-Thumb_0.png"
var detailsDefaultImage = "https://ihfedudev-admin.linkdev.com/sites/default/files/news-images/2022-03/News-Detailslogo_0.png"

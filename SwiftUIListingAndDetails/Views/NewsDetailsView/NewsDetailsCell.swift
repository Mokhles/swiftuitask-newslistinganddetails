//
//  NewsDetailsCell.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 28/02/2023.
//

import SwiftUI

struct NewsDetailsCell: View {

    var newsDetailsResult: NewsDetailsResult
    @State private var webViewHeight: CGFloat = .zero
    
    var body: some View {
        ScrollView {
            VStack {
                AsyncImage(url: URL(string: newsDetailsResult.newsImageURL ?? detailsDefaultImage)) { image in
                    image.resizable()
                } placeholder: {
                    ProgressView()
                }
                .frame(height: 217)
                .aspectRatio(contentMode: .fit)
                VStack(alignment: .leading) {

                    Text(newsDetailsResult.newsTitle ?? "")
                        .foregroundColor(Color(hex: "#333333"))
                        .font(
                            .custom(
                                "Georgia",
                                fixedSize: 15))
                    Text("\(DateFormatter().date(fromString: newsDetailsResult.newsDate ?? "", withFormat: .yyyyMMdd)?.addingTimeInterval(600) ?? Date(), style: .date)")
                        .foregroundColor(Color(hex: "#244BA6"))
                        .font(
                            .custom(
                                "Georgia",
                                fixedSize: 10))
                    Spacer()
                    if newsDetailsResult.newsDescription != nil {
                        Webview(text: newsDetailsResult.newsDescription ?? "", dynamicHeight: $webViewHeight)
                            .padding(.horizontal, -5.0)
                            .frame(height: webViewHeight)

                    }
                    Spacer()
                }
                .padding(.all, 20.0)
                .ignoresSafeArea()
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .environment(\.layoutDirection, .leftToRight)

    }
}

struct NewsDetailsCell_Previews: PreviewProvider {
    static var previews: some View {
        NewsDetailsCell(newsDetailsResult: MockDetailsData.NewsDatails)
    }
}

//
//  NewsDetailsView.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 28/02/2023.
//

import SwiftUI

struct NewsDetailsView: View {

    var id: Int
    @StateObject private var  viewModel = NewsDetailsViewModel()

    var body: some View {
        NewsDetailsCell(newsDetailsResult: viewModel.detailsResults ?? NewsDetailsResult())
        
        // onAppear
            .onAppear {
                viewModel.getNewsDetails(id: id)
            }

        // Alert
            .alert(item: $viewModel.alertItem) { alertItem in
                Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
            }
    }
}


struct NewsDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        NewsDetailsView(id: 731)
    }
}




//
//  SwiftUIListingAndDetailsApp.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 26/02/2023.
//

import SwiftUI

@main
struct SwiftUIListingAndDetailsApp: App {
    var body: some Scene {
        WindowGroup {
            NewsListView()
        }
    }
}

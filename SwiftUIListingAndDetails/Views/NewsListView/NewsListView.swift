//
//  NewsListView.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 26/02/2023.
//

import SwiftUI

struct NewsListView: View {

    init() {
        //Use this if NavigationBarTitle is with Large Font
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : UIFont(name: "Georgia-Bold", size: 40)!]
    }

    @StateObject private var  viewModel = NewsListViewModel()

    var body: some View {
        NavigationView {
            List(viewModel.results, id: \.newsID) { item in
                NewsCell(news: item)
                    .padding(-5)
            }
            .background(Color(hex: "#F5F5F5"))
            .scrollContentBackground(.hidden)
            .listStyle(GroupedListStyle())
            .environment(\.defaultMinListRowHeight, 1) //minimum row height
            .navigationTitle("news")
            .simultaneousGesture(DragGesture().onChanged({ value in
                if value.translation.height > 0 {
                    print("Scroll down")
                } else {
                    viewModel.nextPage()
                }
            }))

            // Searchable
            .searchable(text: $viewModel.searchText, prompt: "searchForNews")
            .onChange(of: viewModel.searchText) { searchText in
                viewModel.searchFor(query: searchText)
            }
        }
        // onAppear
        .onAppear {
            viewModel.getNews()
        }

        // Alert
        .alert(item: $viewModel.alertItem) { alertItem in
            Alert(title: alertItem.title, message: alertItem.message, dismissButton: alertItem.dismissButton)
        }
    }
}

struct DetailView: View {
    var details: String
    var body: some View {
        Text(details)
            .font(.body)
            .fontWeight(.regular)
            .multilineTextAlignment(.center)
            .padding(.all, 25.0)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NewsListView()
    }
}


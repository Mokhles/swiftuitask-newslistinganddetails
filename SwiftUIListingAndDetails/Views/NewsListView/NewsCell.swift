//
//  NewsCell.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 27/02/2023.
//

import SwiftUI

struct NewsCell: View {

    var news: Result

//    static let stackDateFormat: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        return formatter
//    }()

    
    var body: some View {
        ZStack(alignment: .leading) {
            HStack {
                AsyncImage(url: URL(string: news.newsImageURL ?? thumbdDefaultImage)) { image in
                    image.resizable()
                } placeholder: {
                    ProgressView()
                }
                .frame(width: 112, height: 112)
                .aspectRatio(contentMode: .fill)

                VStack(alignment: .leading, spacing: 9.0) {

                    Text("\(DateFormatter().date(fromString: news.newsDate ?? "", withFormat: .yyyyMMdd)?.addingTimeInterval(600) ?? Date(), style: .date)")
                        .foregroundColor(Color(hex: "#244BA6"))
                        .font(.custom(
                            "Georgia",
                            fixedSize: 10))
                    Text(news.newsTitle ?? "")
                        .foregroundColor(Color(hex: "#333333"))
                        .font(.custom(
                            "Georgia",
                            fixedSize: 14))
                    Spacer()
                }
                .padding(.top, 15.0)
            }

            .frame(height: 112)

            NavigationLink(destination: NewsDetailsView(id: Int(news.newsID ?? "") ?? -1)) {
            }
            .opacity(0.0)
            .buttonStyle(PlainButtonStyle())
        }
        .background(Color.white)
        .cornerRadius(10)
        .listRowBackground(Color(hex: "#F5F5F5")) // to change backgroud list
        .listRowSeparator(.hidden)
    }
}

struct NewsCell_Previews: PreviewProvider {
    static var previews: some View {

        Group {
            NewsCell(news: MockData.News[0])
            NewsCell(news: MockData.News[1])
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}

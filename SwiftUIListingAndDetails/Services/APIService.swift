//
//  APIService.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 28/02/2023.
//

import Foundation

class APIService: NSObject {

    //Staic iConstant
    static let shared   = APIService()
    static let baseURL  = "https://ihfeducation-admin.ihf.info"
    //"https://ihfedudev-admin.linkdev.com"

    //Private iConstant
    private let newsURL = baseURL + "/api/v1/news"

    // MARK: - getNews
    func getNews(pageIndex: Int, completion: @escaping(Swift.Result<News, APError>) -> Void){
        APIService.shared.request(url: newsURL + "?pageIndex=\(pageIndex)&pageSize=\(kAPIsPageSize)", completed: completion)
    }

    // MARK: - getNewsDetails
    func getNewsDetails(id: Int, completion: @escaping(Swift.Result<NewsDetails, APError>) -> Void){
        APIService.shared.request(url: newsURL + "/\(id)", completed: completion)
    }

    // MARK: - APIService
    func request<T: Decodable>(url: String, completed: @escaping (Swift.Result<T, APError>) -> Void) {
        guard let url = URL(string: url) else {
            completed(.failure(.invalidURL))
            return
        }

        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { data, response, error in

            if let _ =  error {
                completed(.failure(.unableToComplete))
                return
            }

            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completed(.failure(.invalidResponse))
                return
            }

            guard let data = data else {
                completed(.failure(.invalidData))
                return
            }

            do {
                let decoder = JSONDecoder()
                let decodedResponse = try decoder.decode(T.self, from: data)
                completed(.success(decodedResponse.self))
            } catch {
                completed(.failure(.invalidData))
            }
        }
        task.resume()
    }
}

//
//  NewsListDetailsViewModel.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 28/02/2023.
//

import Foundation


final class NewsDetailsViewModel: ObservableObject {
    
    @Published var alertItem: AlertItem?
    @Published var detailsResults: NewsDetailsResult?
    
    func getNewsDetails(id: Int) {
        
        APIService.shared.getNewsDetails(id: id) { [self] result in
            DispatchQueue.main.async { [self] in
                
                switch result {
                case .success(let newsDetails):
                    self.detailsResults = newsDetails.result
                    
                case .failure(let error):
                    switch error {
                    case .invalidData:
                        alertItem = AlertContext.invalidData
                        
                    case .invalidURL:
                        alertItem = AlertContext.invalidURL
                        
                    case .invalidResponse:
                        alertItem = AlertContext.invalidResponse
                        
                    case .unableToComplete:
                        alertItem = AlertContext.unableToComplete
                    }
                }
            }
        }
    }
}

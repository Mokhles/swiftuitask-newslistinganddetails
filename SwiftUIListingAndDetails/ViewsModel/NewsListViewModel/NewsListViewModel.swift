//
//  NewsListViewModel.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 28/02/2023.
//

import Foundation

final class NewsListViewModel: ObservableObject  { // with final keyword you cantt create subclass from this class
    
    @Published var results = [Result]() // Published when this variable is changed it will update automatically .. when changes get made swiftui will update ui
    @Published var resultsWithoutFilteration = [Result]()
    @Published var searchText = ""
    @Published var alertItem: AlertItem?

    // MARK: -  Private iVars

    private(set) var pageIndex: Int = 0
    private(set) var news = News()

    // MARK: -  iVars

    var newsCount: Int  {
        return news.totalcount ?? 0
    }

    // MARK: -  iFunctions

    func getNews(atPage: PageIndex = .first) {

        if atPage == .first {
            pageIndex = 0
        } else {
            pageIndex += 1
        }

        APIService.shared.getNews(pageIndex: pageIndex) { [self] result in
            DispatchQueue.main.async { [self] in

                switch result {
                case .success(let news):

                    self.news = news

                    if atPage == .first {
                        self.results = news.result ?? []
                        self.resultsWithoutFilteration = news.result ?? []
                    } else {
                        self.results.append(contentsOf: news.result ?? [])
                        self.resultsWithoutFilteration.append(contentsOf: news.result ?? [])
                    }

                case .failure(let error):
                    switch error {
                    case .invalidData:
                        alertItem = AlertContext.invalidData
                        
                    case .invalidURL:
                        alertItem = AlertContext.invalidURL
                        
                    case .invalidResponse:
                        alertItem = AlertContext.invalidResponse
                        
                    case .unableToComplete:
                        alertItem = AlertContext.unableToComplete
                    }
                }
            }
        }
    }

    func nextPage() {
        guard results.count < newsCount else {return}
        getNews(atPage: .next)
    }

    func searchFor(query: String) {
        if !query.isEmpty {
            results = resultsWithoutFilteration.filter { $0.newsTitle?.contains(searchText) ?? false
            }
        } else {
            results = resultsWithoutFilteration
        }
    }
}

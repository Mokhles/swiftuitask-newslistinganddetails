//
//  News.swift
//  SwiftUIListingAndDetails
//
//  Created by Mark Mokhles on 26/02/2023.
//

import Foundation

// MARK: - News

struct News: Identifiable, Decodable {
    var id: UUID?

    var status: Int?
    var message: String?
    var totalcount: Int?
    var result: [Result]?
}

// MARK: - Result

struct Result: Decodable {
    var newsID, newsTitle, newsBrief, newsDate: String?
    var newsImageURL: String?

    enum CodingKeys: String, CodingKey {
        case newsID = "news_id"
        case newsTitle = "news_title"
        case newsBrief = "news_brief"
        case newsDate = "news_date"
        case newsImageURL = "news_image_url"
    }
}

// MARK: - MockData

struct MockData {
    static let News = [sampleNews, sampleNews1]
    static let sampleNews = Result(newsID: "728",
                                   newsTitle: "IHF Privacy Policy Update",
                                   newsBrief: "The new General Data Protection Regulation (GDPR) of the European Union has come into force on 25 May 2018.",
                                   newsDate: "2018-05-29",
                                   newsImageURL: "http://ihfeducation-admin.ihf.info/sites/default/files/news-images/2022-03/policy-thumb.jpg")

    static let sampleNews1 = Result(newsID: "729",
                                    newsTitle: "Sign up for the IHF Coaches Symposium!",
                                    newsBrief: "Since the previous IHF Coaches Symposium during the 2015 Men’s World Championship in Qatar was an outstanding success",
                                    newsDate: "2016-11-18",
                                    newsImageURL: "http://ihfeducation-admin.ihf.info/sites/default/files/news-images/2022-03/Coaches%20Symposium-thumb.jpg")
}

